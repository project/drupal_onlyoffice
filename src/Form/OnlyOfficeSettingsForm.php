<?php

namespace Drupal\drupal_onlyoffice\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\HtmlCommand;

use Drupal\drupal_onlyoffice\OOConnector;

/**
 * Configure example settings for this site.
 */
class OnlyOfficeSettingsForm extends ConfigFormBase {
    /** @var string Config settings */
  const SETTINGS = 'drupal_onlyoffice.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drupal_onlyoffice_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $config = $this->config(static::SETTINGS);

    $form['oo_lw_server_adress'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server adress'),
      '#description' => $this->t('ex: http://210.0.0.2'),
      '#default_value' => $config->get('oo_lw_server_adress'),
      '#required' => TRUE,
    ];  

    $form['oo_lw_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OnlyOffice user'),
      '#default_value' => $config->get('oo_lw_user'),
      '#description' => $this->t('Your onlyOffice user'),
      '#required' => TRUE,
    ];  

    $form['oo_lw_user_password'] = [
      '#type' => 'password',
      '#title' => $this->t('User password'),
      '#default_value' => $config->get('oo_lw_user_password'),
    ];  

    $form['oo_help'] = [
        '#type' => 'item',
        '#title' => t('Server response'),
        '#markup' => '<p>'.$this->t('Click Get Token').'</p>',
        '#prefix' => '<div id="oo-help-wrapper">',
        '#suffix' => '</div>',        
    ];

    if (!empty( $config->get('oo_lw_token') )) 
    {
        $form['oo_help']['#markup'] = '<p>'.$this->t('Existing token').' : '.$config->get('oo_lw_token').'</p>';
    }        

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
        '#type' => 'submit',
        '#value' => $this->t('Get token'),
        '#attributes' => [
            'class' => [ 'use-ajax' ],
        ],
        '#ajax' => [
            'callback' => [$this, 'submitFormAjax'],
            'event' => 'click',
        ],
    ];

    return $form;
  }

	/**
	 * Form validation
	 * @param array $form
	 * @param FormStateInterface $form_state
	 */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {

		$form_state->clearErrors();

        if( empty($form_state->getValue('oo_lw_server_adress')) )
        {
            $form_state->setErrorByName('oo_lw_server_adress', $this->t('Server adress'));
        }
        if( empty($form_state->getValue('oo_lw_user')) )
        {
            $form_state->setErrorByName('oo_lw_user', $this->t('Username'));
        }

    }

	/**
	 * AJAX callback handler that displays any errors or a success message.
	 */
	public function submitFormAjax(array &$form, FormStateInterface $form_state)
	{
        $response = new AjaxResponse();
        
        // Init connector
        $connector = new OOConnector();
        
        // Try to get the access token
        $token = $connector->get_access_token(
            $form_state->getValue('oo_lw_server_adress'), 
            $form_state->getValue('oo_lw_user'), 
            $form_state->getValue('oo_lw_user_password')
        );

        // Ok
        if(isset($token->token)) 
        {
            // Retrieve the configuration
            $this->configFactory->getEditable(static::SETTINGS)
                // Set the submitted configuration setting
                ->set('oo_lw_server_adress', $form_state->getValue('oo_lw_server_adress'))
                ->set('oo_lw_user', $form_state->getValue('oo_lw_user'))
                ->set('oo_lw_token', $token->token)
                ->save();
        }        
        
        $response->addCommand(new HtmlCommand('#oo-help-wrapper', '<pre>'.print_r($token, true).'</pre>' ));
        return $response;
        
	}

    /** 
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {}
}
