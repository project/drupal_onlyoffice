<?php
/**
 * @file
 * Contains \Drupal\drupal_onlyoffice\Plugin\Block\UserFilesBlock.
 */

namespace Drupal\drupal_onlyoffice\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Component\Serialization\Json;
use Drupal\drupal_onlyoffice\OOConnector;

/**
 * Provides a 'Modal' Block
 *
 * @Block(
 *   id = "oo_userfiles_block",
 *   admin_label = @Translation("OnlyOffice user files"),
 * )
 */
class UserFilesBlock extends BlockBase {
    /**
     * {@inheritdoc}
     */
    public function build() 
    {
        $connector = new OOConnector();

        $config = \Drupal::config('drupal_onlyoffice.settings');
        $tree = $connector->get_user_files($config->get('oo_lw_server_adress'), $config->get('oo_lw_token'));
        $files = [];

        // Erreur
        if (!is_array($tree) AND isset($tree->response) AND !empty($tree->response)) 
        {
            // Construction de la sortie
            foreach ($tree->response as $type => $element) 
            {
                switch ($type) 
                {
                    // Fichiers
                    case 'files':
                        if (empty($element)) continue;

                        foreach ($element as $file) 
                        {
                            $files[] = [
                                'id' => $file->id,
                                'title' => $file->title,
                                'created' => $file->created,
                                'createdBy' => $file->createdBy->displayName,
                                'updated' => $file->updated,
                                'updatedBy' => $file->updatedBy->displayName,
                                'webUrl' => $file->webUrl,
                            ];
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        return [
            '#theme' => 'drupal_onlyoffice_user_files',
            '#files' => $files
        ];
    }
}