<?php

/**
* @file fournit un service de synchronisation avec OnlyOffice
*
*/

namespace Drupal\drupal_onlyoffice;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\Node;

use GuzzleHttp\Exception\RequestException;


class OOConnector {

	private $oo_server;
	private $oo_admin_user;
	private $oo_admin_passwd;
	private $oo_token;
	public $ready;

	private $oo_api_actions = [
		'auth' => [
			'url' => '%s/api/2.0/authentication',
			'Content-Type' => 'application/json',
			'Accept' => 'application/json'
		],
		'get_user_files' => [
			'url' => '%s/api/2.0/files/@my',
			'Content-Type' => 'application/json',
			'Accept' => 'application/json'
		],

	];

	// Get Token
	public function get_access_token($server, $username, $password)
	{
		// Construction de l'url
		$url = sprintf($this->oo_api_actions['auth']['url'], $server);

		try 
		{
			//Envoi de la demande d'un token d'authentification
			$response = \Drupal::httpClient()->post($url, [
				'http_errors' => FALSE,
				'headers' => [
					'Content-Type' => $this->oo_api_actions['auth']['Content-Type'],
					'Accept' => $this->oo_api_actions['auth']['Accept'],
				],
				'body' => json_encode([
					'userName' => $username,
					'password' => $password,
				]),
			]);

			$response_data = json_decode($response->getBody()->getContents());

			// Authentification OK
			if ($response->getStatusCode() == '201')
			{			 	
			 	return $response_data->response;
			} 
			else
			{
				$this->oo_token = null;
				return [
					'StatusCode' => $response->getStatusCode(),
					'ReasonPhrase' => $response->getReasonPhrase(),
					'Message' => $response_data,
				];
			}
		}
		catch (RequestException $e) 
		{
			watchdog_exception('OOConnector::get_token', $e);
			\Drupal::logger('OOConnector::get_token')->error($url);
			$this->oo_token = null;
			return false;
		}
	}


	// Récupère l'arborescence d'un dossier
	public function get_user_files($server, $token)
	{
		// Construction de l'url
		$url = sprintf($this->oo_api_actions['get_user_files']['url'], $server);

		//Envoi de la demande d'un token d'authentification
		$response = \Drupal::httpClient()->get($url, [
			'http_errors' => FALSE,
			'headers' => [
				'Content-Type' => $this->oo_api_actions['get_user_files']['Content-Type'],
				'Accept' => $this->oo_api_actions['get_user_files']['Accept'],
				'Authorization' => $token,
			]
		]);

		$response_data = json_decode($response->getBody()->getContents());

		// Création du groupe Ok
		if ($response->getStatusCode() == '200')
		{		 	
		 	return $response_data;
		} 
		else
		{
			return [
				'StatusCode' => $response->getStatusCode(),
				'ReasonPhrase' => $response->getReasonPhrase(),
				'Message' => $response_data,
			];			
		}
	}
}